// The MIT License(MIT)
//
// Copyright(c) 2014 Christian Ewald
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#pragma once

// local
#include "TypeInfo.h"

// STL
#include <memory>
#include <unordered_map>
#include <list>

// boost
#include <boost/noncopyable.hpp>
#include <boost/functional/hash.hpp>

namespace Sioc
{
namespace Detail
{

// local forwards
class IActivator;

/// <summary>
/// The registry mapping registered types to their activators.
/// </summary>
class ActivatorRegistry : private boost::noncopyable
{
public:
    /// <summary>
    /// Collection of activators.
    /// </summary>
    typedef std::list<std::shared_ptr<const Detail::IActivator>> Activators;

private:
    typedef std::unordered_map<Detail::TypeInfo::IdType, Activators, boost::hash<Detail::TypeInfo::IdType>> Registry;

public:
    /// <summary>
    /// Inserts an activator for the specified identifier.
    /// </summary>
    /// <param name="id">The type identifier.</param>
    /// <param name="activator">The activator.</param>
    void Insert(const TypeInfo::IdType& id, const std::shared_ptr<const IActivator>& activator)
    {
        registry_[id].push_back(activator);
    }

    /// <summary>
    /// Gets the activators for a specified type.
    /// </summary>
    /// <param name="id">The type identifier.</param>
    /// <returns>The activators.</returns>
    const Activators& GetAllForType(const TypeInfo::IdType& id) const
    {
        auto found = registry_.find(id);

        if (found != registry_.end())
        {
            return found->second;
        }

        return empty_;
    }

private:
    Registry registry_;
    Activators empty_;
};

}
}